package main

import (
	"bufio"
	"crypto/tls"
	"flag"
	"fmt"
	"github.com/mxk/go-imap/imap"
	"os"
	"time"
)

func doList(c *imap.Client) {
	// List all top-level mailboxes, wait for the command to finish
	cmd, err := imap.Wait(c.List("", "%")); if err != nil {
		fmt.Fprintf(os.Stderr, "failed to perform mailbox list: %s\n",
			err.Error())
		os.Exit(1)
	}

	// Print mailbox information
	fmt.Println("\nTop-level mailboxes:")
    var rsp *imap.Response
	for _, rsp = range cmd.Data {
		fmt.Println("|--", rsp.MailboxInfo())
	}
}

func writeMessage(msgId uint32, internalDate string,
		header string, text string) {
	fileName := fmt.Sprintf("%06d.%s.mail", msgId, internalDate)
	fp, err := os.Create(fileName); if err != nil {
		fmt.Fprintf(os.Stderr, "error creating file %s: %s\n",
				fileName, err.Error())
		os.Exit(1)
	}
	defer func() {
		e := fp.Close(); if e != nil {
			fmt.Fprintf(os.Stderr, "error closing file %s: %s\n",
					fileName, err.Error())
			os.Exit(1)
		}
	}()
	w := bufio.NewWriter(fp)
	_, err = w.WriteString(header); if err != nil {
		fmt.Fprintf(os.Stderr, "error writing header to file %s: %s\n",
				fileName, err.Error())
		os.Exit(1)
	}
	_, err = w.WriteString(text); if err != nil {
		fmt.Fprintf(os.Stderr, "error writing message body to file %s: %s\n",
				fileName, err.Error())
		os.Exit(1)
	}
	err = w.Flush(); if err != nil {
		fmt.Fprintf(os.Stderr, "error flushing data to file %s: %s\n",
				fileName, err.Error())
		os.Exit(1)
	}
}

func doSnarf(c *imap.Client, mailboxName *string) {
	// Check for new unilateral server data responses
    var rsp *imap.Response
	for _, rsp = range c.Data {
		fmt.Printf("Server data: %s\n", rsp)
	}
	c.Data = nil
	_, err := c.Select("INBOX", true); if err != nil {
		fmt.Fprintf(os.Stderr, "failed to open mailbox %s: %s\n",
			*mailboxName, err.Error())
	}

	fmt.Printf("Mailbox status:%s\n", c.Mailbox)
	fmt.Printf("There are %d messages in this mailbox.\n", c.Mailbox.Messages)
	totalMessages := c.Mailbox.Messages
	batchSize := uint32(10)
	for idx := uint32(1); idx <= totalMessages; idx += batchSize {
		fmt.Printf("fetching messages %05d through %05d...\n",
			idx, idx + batchSize)
		set := &imap.SeqSet{}
		set.AddRange(idx, idx + batchSize)
		var cmd *imap.Command
		cmd, err = c.Fetch(set, "INTERNALDATE", "RFC822.HEADER", "RFC822.TEXT")
		if err != nil {
			fmt.Fprintf(os.Stderr, "failed to issue FETCH command: %s\n",
				err.Error())
			os.Exit(1)
		}
		for cmd.InProgress() {
			// Wait for the next response (no timeout)
			err := c.Recv(-1); if err != nil {
				fmt.Fprintf(os.Stderr, "Recv failed: did the server close " +
					"the connection?  error: %s\n", err.Error())
				os.Exit(1)
			}
			// Process command data
			for _, rsp = range cmd.Data {
				msgId := rsp.MessageInfo().Seq
				attrs := rsp.MessageInfo().Attrs
				writeMessage(msgId,
					imap.AsString(attrs["INTERNALDATE"]),
					imap.AsString(attrs["RFC822.HEADER"]),
					imap.AsString(attrs["RFC822.TEXT"]))
			}
		}
		// Check command completion status
		if rsp, err := cmd.Result(imap.OK); err != nil {
			if err == imap.ErrAborted {
				fmt.Println("The fetch command was aborted.")
			} else {
				fmt.Println("There was a fetch error:", rsp.Info)
			}
		}
	}
//	// Fetch the headers of the ten most recent messages
//	var set *imap.SeqSet = &imap.SeqSet{}
//	if c.Mailbox.Messages >= 10 {
//		set.AddRange(c.Mailbox.Messages-9, c.Mailbox.Messages)
//	} else {
//		set.Add("1:*")
//	}
//	var cmd *imap.Command
//	cmd, err = c.Fetch(set, "RFC822.HEADER", "RFC822.TEXT"); if err != nil {
//		fmt.Fprintf(os.Stderr, "failed to fetch RFC822 header: %s\n",
//			err.Error())
//		os.Exit(1)
//	}
//	// Process responses while the command is running
//	fmt.Println("\nMost recent messages:")
//	for cmd.InProgress() {
//		// Wait for the next response (no timeout)
//		c.Recv(-1)
//		// Process command data
//		for _, rsp = range cmd.Data {
//			fmt.Printf("%s\n", imap.AsString(rsp.MessageInfo().Attrs["RFC822.HEADER"]))
//			//header := imap.AsBytes(rsp.MessageInfo().Attrs["RFC822.HEADER"])
////			if msg, _ := imap.ReadMessage(bytes.NewReader(header)); msg != nil {
////				fmt.Println("|--", msg.Header.Get("Subject"))
////			}
//		}
//		cmd.Data = nil
//		// Process unilateral server data
//		for _, rsp = range c.Data {
//			fmt.Println("Server data:", rsp)
//		}
//		c.Data = nil
//	}
//	// Check command completion status
//	if rsp, err := cmd.Result(imap.OK); err != nil {
//		if err == imap.ErrAborted {
//			fmt.Println("The fetch command was aborted.")
//		} else {
//			fmt.Println("There was a fetch error:", rsp.Info)
//		}
//	}
}

func main() {
	serverName := flag.String("server", "imap.gmail.com:993",
		"the IMAP server to connect to.")
	userName := flag.String("user", "me@gmail.com",
		"the user name to use.")
	password := flag.String("password", "123",
		"the password to use.")
	mailboxName := flag.String("mailbox", "[Gmail]/All Mail",
		"the mailbox to use.")
	action := flag.String("action", "list",
		"What action to take: list or snarf")
	flag.Parse()
	tlsConfig := &tls.Config{}
	c, err := imap.DialTLS(*serverName, tlsConfig); if err != nil {
		fmt.Fprintf(os.Stderr, "failed to dial imap server: %s\n",
			err.Error())
		os.Exit(1)
	}
	// Log out and close the connection after we're finished.
	defer c.Logout(30 * time.Second)
	fmt.Println("The IMAP server says hello:", c.Data[0].Info)
	c.Data = nil
	if c.Caps["STARTTLS"] {
		// enable encryption, if the server supports it
		_, err := c.StartTLS(nil); if err != nil {
			fmt.Fprintf(os.Stderr, "StartTLS failed: %s\n", err.Error())
			os.Exit(1)
		}
	}
	if c.State() == imap.Login {
		_, err := c.Login(*userName, *password)
		if err != nil {
			fmt.Fprintf(os.Stderr, "failed to login: %s\n", err.Error())
			os.Exit(1)
		}
	}
	if (*action == "list") {
		doList(c)
	} else if (*action == "snarf") {
		doSnarf(c, mailboxName)
	} else {
		fmt.Fprintf(os.Stderr, "unrecognized action: %s\n", *action)
		os.Exit(1)
	}
}
